﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace google_calendar_sync_impl
{
    public partial class frmCredentials : Form
    {
        public string UserName;
        public string Pwd;
        public frmCredentials()
        {
            InitializeComponent();
        }

        private void frmCredentials_Load(object sender, EventArgs e)
        {
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UserName = txtUser.Text.Trim();
            Pwd = txtPwd.Text.Trim();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
