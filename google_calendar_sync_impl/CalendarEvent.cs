﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace google_calendar_sync_impl
{
    public class CalendarEvent
    {
        [XmlAttribute("group")]
        public string Group;

        [XmlAttribute("home")]
        public bool Home;

        [XmlAttribute("category")]
        public string Category;

        [XmlElement(IsNullable = true)]
        public DateTime? EventDate;

        [XmlElement(IsNullable = true)]
        public DateTime? EventDateTo;

        [XmlElement(IsNullable = true)]
        public string EventTime;

        [XmlElement(IsNullable = true)]
        public string EventTimeTo;

        [XmlElement("EventTitle")]
        public object Title;

        [XmlElement("EventLocation")]
        public string Location;

        [XmlElement(IsNullable = true, ElementName = "EventDescription")]
        public object Description;

        [XmlElement("Program")]
        public object Program;

        public CalendarEvent()
        {
        }
    }

}
