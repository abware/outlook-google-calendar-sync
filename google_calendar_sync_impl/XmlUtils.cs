﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace google_calendar_sync_impl
{
    public class XmlUtils
    {
        public static string ToString(object o)
        {
            if (o == null)
            {
                return null;
            }
            else if (o is XmlNode[])
            {
                StringBuilder builder = new StringBuilder();
                foreach (XmlNode node in (XmlNode[])o)
                {
                    builder.Append(node.OuterXml);
                }

                return builder.ToString();
            }
            else if (o.GetType().Name.Equals("Object"))
            {
                return "";
            }
            else
            {
                return o.ToString();
            }
        }
    }
}
