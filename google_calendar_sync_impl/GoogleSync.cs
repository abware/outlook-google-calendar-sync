﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;
using Google.GData.Calendar;
using Google.GData.Extensions;
using Google.GData.Client;
using System.Data;

namespace google_calendar_sync_impl
{
    public class GoogleSync
    {

        private Uri calendarUri = null;
        private string appName = "google_outlook_calendar_sync-0.0.1";
        private string userName = "";
        private string password = "";
        private CalendarService service = null;

        private SQLiteConnection cnn = null;

        public bool init()
        {

            frmCredentials f = new frmCredentials();
            if (f.ShowDialog() != System.Windows.Forms.DialogResult.OK) return false;
            userName = f.UserName;
            password = f.Pwd;
            calendarUri = new Uri("https://www.google.com/calendar/feeds/" + userName + "/private/full");
            service = new CalendarService(appName);
            service.setUserCredentials(userName, password);


            string dataPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\googleCalendarSync.db";
            string conStr = "Data Source=" + dataPath + ";Pooling=true;FailIfMissing=false";
            cnn = new SQLiteConnection(conStr);
            cnn.Open();
            execSql("CREATE TABLE IF NOT EXISTS syncItems (entityID TEXT PRIMARY KEY ASC, startDate INTEGER, atomUri TEXT)");
            
            return true;
        }

        public bool isSync(string entryID)
        {
            return getScalar("SELECT entityID FROM syncItems WHERE entityID='" + entryID + "'") != null;
        }


        private object getScalar(string sql)
        {
            try
            {
                SQLiteCommand cmd = cnn.CreateCommand();
                cmd.CommandText = sql;
                return cmd.ExecuteScalar();
            }
            catch
            {
                return null;
            }

        }

        private bool execSql(string sql)
        {
            try
            {
                SQLiteCommand cmd = cnn.CreateCommand();
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
            }
            catch
            {
                return false;
            }

            return true;
        }

        private bool execSql(string sql, out DataTable dt )
        {
            try
            {
                DataSet ds = new DataSet();
                SQLiteCommand cmd = cnn.CreateCommand();
                cmd.CommandText = sql;
                SQLiteDataAdapter oDa = new SQLiteDataAdapter();
                oDa.SelectCommand = cmd;
                oDa.Fill(ds);
                dt = ds.Tables[0];
            }
            catch
            {
                dt = null;
                return false;
            }

            return true;
        }


        public void sync(string entryID, CalendarEvent ce)
        {
            string uri = string.Empty;

            if (InsertEntry(ce, out uri))
            {
                execSql("INSERT INTO syncItems (entityID, startDate, atomUri) VALUES ('" + 
                    entryID +
                    "'," + ce.EventDate.Value.Ticks.ToString() +
                    ",'" + uri + "')");
            }

        }


        private bool InsertEntry(CalendarEvent e, out string atomUri)
        {
            try
            {
                EventEntry entry = new EventEntry();

                entry.Title.Text = XmlUtils.ToString(e.Title);

                string htmlDescription = XmlUtils.ToString(e.Description);
                if (htmlDescription != null && htmlDescription.Length > 0)
                {
                    entry.Content.Type = "html";
                    entry.Content.Content = htmlDescription;
                }


                if (e.Location != null && e.Location.Length > 0)
                {
                    Where eventLocation = new Where();
                    eventLocation.ValueString = e.Location;
                    entry.Locations.Add(eventLocation);
                }


                string text = e.EventDate.Value.ToShortDateString() + " " + e.EventTime;
                DateTime start = DateTime.Parse(text.Trim());

                When eventTime = new When();
                eventTime.StartTime = start;

                if (e.EventTimeTo != null && e.EventTimeTo.Length > 0)
                {
                    if (e.EventDateTo.HasValue)
                    {
                        text = e.EventDateTo.Value.ToShortDateString() + " " + e.EventTimeTo;
                    }
                    else
                    {
                        text = e.EventDate.Value.ToShortDateString() + " " + e.EventTimeTo;
                    }

                    DateTime endTime = DateTime.Parse(text.Trim());
                    eventTime.EndTime = endTime;
                }
                else if (e.EventDateTo.HasValue)
                {
                    eventTime.EndTime = e.EventDateTo.Value;
                }

                entry.Times.Add(eventTime);



                if ((e.EventTime == null || e.EventTime.Length == 0) &&
                   (e.EventTimeTo == null || e.EventTimeTo.Length == 0))
                {
                    eventTime.AllDay = true;
                }


                //AtomPerson author = new AtomPerson(AtomPersonType.Author);
                //author.Name = "Simon Williams";
                //author.Email = "simon@kajabity.com";
                //entry.Authors.Add(author);

                AtomEntry insertedEntry = service.Insert(calendarUri, entry);
                atomUri = insertedEntry.SelfUri.ToString();
            }
            catch
            {
                atomUri = null;
                return false;
            }

            return true;
        }


        public void cleanUp(long startDate, Dictionary<string, object> col)
        {
            DataTable dt = new DataTable();
            execSql("SELECT entityID, atomUri FROM syncItems WHERE startDate>=" + startDate.ToString(), out dt);
            foreach (DataRow r in dt.Rows)
            {
                if (!col.ContainsKey(r[0].ToString()))
                {
                    //service.Delete(new Uri(r[1].ToString()));
                    FeedQuery singleQuery = new FeedQuery();
                    singleQuery.Uri = new Uri(r[1].ToString()); 
                    AtomFeed newFeed = service.Query(singleQuery);
                    AtomEntry retrievedEntry = newFeed.Entries[0];
                    retrievedEntry.Delete();
                    execSql("DELETE FROM syncItems WHERE entityID='" + r[0].ToString() + "'");
                }
            }
        }

    }
}
