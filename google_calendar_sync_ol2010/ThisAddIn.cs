﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using google_calendar_sync_impl;
using System.Windows.Forms;

namespace google_calendar_sync_ol2010
{
    public partial class ThisAddIn
    {

        private Timer _syncTimer = new Timer();
        private GoogleSync _gsync = new GoogleSync();

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {

            if (_gsync.init())
            {
                _syncTimer.Interval = 10000;
                _syncTimer.Tick += new EventHandler(_syncTimer_Tick);
                _syncTimer.Enabled = true;
                _syncTimer.Start();
            }

        }


        void _syncTimer_Tick(object sender, EventArgs e)
        {
            _syncTimer.Enabled = false;


            Outlook.MAPIFolder calendar = Application.Session.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderCalendar);
            Outlook.Items calendarItems = calendar.Items;

            //MessageBox.Show(calendarItems.Count.ToString());


            int extraWeeks = 3;
            DateTime today = DateTime.Today;
            DateTime firstDayOfWeek = today.AddDays((int)today.DayOfWeek * -1);
            firstDayOfWeek.Subtract(new TimeSpan(today.Hour, today.Minute, today.Second));
            DateTime lastDayOfWeek = firstDayOfWeek.Add(new TimeSpan(6, 23, 59, 59));
            for (int i = 0; i < extraWeeks; ++i)
            {
                lastDayOfWeek = lastDayOfWeek.Add(new TimeSpan(7, 0, 0, 0));
            }
            

            //Outlook.AppointmentItem item = calendarItems["Test Appointment"] as Outlook.AppointmentItem;
            foreach (object o in calendarItems)
            {
                Outlook.AppointmentItem item = o as Outlook.AppointmentItem;
                if (item != null && item.Start >= firstDayOfWeek && item.End <= lastDayOfWeek && !_gsync.isSync(item.EntryID))
                {
                    CalendarEvent ce = new CalendarEvent();
                    ce.Title = item.Subject;
                    ce.Description = item.Body;
                    ce.Location = item.Location;
                    ce.EventDate = item.Start;
                    ce.EventDateTo = item.End;

                    ce.EventTime = item.Start.ToShortTimeString();
                    ce.EventTimeTo = item.End.ToShortTimeString();

                    _gsync.sync(item.EntryID, ce);
                }

            }


            //Outlook.RecurrencePattern pattern =
            //    item.GetRecurrencePattern();
            //Outlook.AppointmentItem itemDelete = pattern.
            //    GetOccurrence(new DateTime(2006, 6, 28, 8, 0, 0));

            //if (itemDelete != null)
            //{
            //    itemDelete.Delete();
            //}

            _syncTimer.Enabled = true;
        }



        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }


        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }

        #endregion
    }
}
